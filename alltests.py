import unittest
from testcritterbasic import TestCritter
from testworld import TestWorld
from critterclusteringtest import TestCritterClustering
from reproductiontest import TestReproduction

suite = unittest.TestSuite()
suite.addTest(unittest.makeSuite(TestCritter))
suite.addTest(unittest.makeSuite(TestWorld))
suite.addTest(unittest.makeSuite(TestCritterClustering))
suite.addTest(unittest.makeSuite(TestReproduction))

runner = unittest.TextTestRunner()
runner.run(suite)
