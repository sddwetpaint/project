import sys, pygame, random
from pygame.locals import *
from button import ClickedButton
from critter import Critter
from fileButtonInherited import *
from screen import Screen
from slider import Slider
from text import JustText
from toggle import SimpleToggle
from window import Window
from world import World

# Initialize PyGame
pygame.init()

# Make the main surface (this should only be made once and is constant)
SCREEN = Screen(1200, 600)
SCREEN.changeColor((177, 148, 124))

'''
Create (default) world
'''
# Make world
gameWindow = Window(998, 577, 1, 22, (177, 148, 124))
world = World()

# Initialize default world with one male and one female critter
randGenome1 = world.generateGenome()
randGenome2 = world.generateGenome()
a = Critter(next(world.maleNameGenerator), randGenome1)
b = Critter(next(world.femaleNameGenerator), randGenome2)
a.gender = 'male'
b.gender = 'female'
world.addCritter(a)
world.addCritter(b)

# Add the world to the gameWindow and add the gameWindow to the surface
gameWindow.addItem(world)
SCREEN.addItem(gameWindow)

'''
Buttons
'''
# Adds the new, open, and save file buttons
newFileBtn = NewFileButton(60, 20, 1, 1, (120, 190, 172), world, SCREEN, 'New')
SCREEN.addItem(newFileBtn)
openFileBtn = OpenFileButton(60, 20, 62, 1, (109, 151, 162), world, SCREEN, 'Open')
SCREEN.addItem(openFileBtn)
saveFileBtn = SaveFileButton(60, 20, 123, 1, (109, 150, 162), world, SCREEN, 'Save')
SCREEN.addItem(saveFileBtn)

# Adds the new critter button
critterBtn = ClickedButton(190, 40, 1000, 418, (109, 151, 162), world, SCREEN, 'Add New Critter', textSize=16)
SCREEN.addItem(critterBtn)
# Adds the play/pause button
pauseBtn = SimpleToggle(94, 40, 1000, 460, (150, 117, 171), world, SCREEN, 'Pause', 'Play', textSize=16)
SCREEN.addItem(pauseBtn)
# Adds the kill button
killBtn = SimpleToggle(94, 40, 1096, 460, (150, 117, 171), world, SCREEN, 'Kill', 'Killing', textSize=16)
SCREEN.addItem(killBtn)

'''
Stat displays
'''
# Adds the world and critter stats windows
critterStatWindow = Window(190, 156, 1000, 259, (120, 190, 172))
critterText = JustText(170, 149, 1000, 264, (120, 190, 172), 'Critter info:')
critterStatWindow.addItem(critterText)
SCREEN.addItem(critterStatWindow)
worldStatWindow = Window(190, 257, 1000, 7, (117, 174, 172))
statsText = JustText(170, 243, 1000, 9, (117, 174, 172), world.getStats())
worldStatWindow.addItem(statsText)
SCREEN.addItem(worldStatWindow)

'''
Sliders
'''
# Adds the speed and sound sliders
speedSlider = Slider('horizontal', 1025, 542, 140, 0, 10, 'speed')
SCREEN.addItem(speedSlider)

'''
Current user session attributes
'''
kill = False
pause = False
gameSpeed = 1
volume = .5


def quitPrompt():
    '''
    Creates a prompt asking if the user wants to save and quit, quit, or cancel
    '''
    pygame.draw.rect(SCREEN.getDisplay(), (117, 174, 172), (450,250,305,50), 0)
    pygame.font.init()
    font = pygame.font.Font("freesansbold.ttf", 16)
    textSurface = font.render("Are you sure you want to quit?", True, (255,255,255))
    SCREEN.getDisplay().blit(textSurface, (475,255))
    savebtn = ClickedButton(95, 15, 455, 280, (255,255,255), world, SCREEN, 'Save and Quit', (0,0,0))
    savebtn.draw(SCREEN.getDisplay())
    quitbtn = ClickedButton(95, 15, 555, 280, (255,255,255), world, SCREEN, 'Quit', (0,0,0))
    quitbtn.draw(SCREEN.getDisplay())
    cancelbtn = ClickedButton(95, 15, 655, 280, (255,255,255), world, SCREEN, 'Cancel', (0,0,0))
    cancelbtn.draw(SCREEN.getDisplay())
    pygame.display.flip()

    cancel = False
    while not cancel:
        for event in pygame.event.get():
            if event.type == MOUSEBUTTONDOWN:
                if cancelbtn.click(pygame.mouse.get_pos()):
                    cancel = True
                if quitbtn.click(pygame.mouse.get_pos()) or savebtn.click(pygame.mouse.get_pos()):
                    if savebtn.click(pygame.mouse.get_pos()):
                        world.saveWorld()
                    pygame.quit()
                    sys.exit()


'''
Gameloop
'''
while True:
    # Draw the world stats
    statsText.setText(world.getStats())
    SCREEN.draw()
    
    for event in pygame.event.get():

        # If the user selects the "x", prompt user to save and quit, quit, or cancel
        if event.type == QUIT:
            quitPrompt()

        # If the event is a click...
        elif event.type == MOUSEBUTTONDOWN:

            # Create a new critter
            c = critterBtn.click(pygame.mouse.get_pos())
            newfish = False
            if c:
                randGenome = world.generateGenome()
                # Choose an available name depending on whether it is male or female
                gender = random.choice(['male', 'female'])
                name = 'breakable banana'
                if gender == 'male':
                    name = next(world.maleNameGenerator, -1)
                    if name == -1:
                        world.maleNameGenerator = world.getValidName('male')
                        world.nameFlag = True
                        name = next(world.maleNameGenerator)
                if gender == 'female':
                    name = next(world.femaleNameGenerator, -1)
                    if name == -1:
                        world.femaleNameGenerator = world.getValidName('female')
                        world.nameFlag = True
                        name = next(world.femaleNameGenerator)
                a = Critter(name, randGenome)
                a.gender = gender
                a.selected = True
                world.addCritter(a)
                newfish = True
                text = world.getCritter(a.name)
                critterText.setText(text)

            # Pause/play game
            p = pauseBtn.click(pygame.mouse.get_pos())
            if p != None:
                pause = not pause
            if pause:
                gameSpeed = 0
            else:
                gameSpeed = speedSlider.getValue()

            # Activate/deactivate kill
            k = killBtn.click(pygame.mouse.get_pos())
            if k != None:
                kill = not kill

            # If the user selects a critter: 
            # Remove critter if kill is activated, otherwise display the critter's stats
            c = world.selectCritter(pygame.mouse.get_pos())
            if c != None:
                if kill:
                    world.killCritter(c)
                else:
                    text = world.getCritter(c)
                    critterText.setText(text)
            # if no new critters were made and the user selected white space, reset the critter info
            elif not newfish:
                critterText.setText('Critter info:')

            # Run prompts for new, open, and save file buttons
            newFileBtn.click(pygame.mouse.get_pos())
            openFileBtn.click(pygame.mouse.get_pos())
            saveFileBtn.click(pygame.mouse.get_pos())

        # Slider control selection
        mousePress = pygame.mouse.get_pressed()
        if mousePress[0]:
            speedSlider.click(pygame.mouse.get_pos())
            if not pause:
                gameSpeed = speedSlider.getValue()

    # Pauses game if the gameSpeed is 0
    if gameSpeed == 0:
        pass

    # Game speed control
    else:
        pygame.time.wait(100 - (gameSpeed * 10))
        world.moveCritters()

    pygame.display.update()
