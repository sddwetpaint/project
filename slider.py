import sys
import pygame
from pygame.locals import *

class Slider(object):
    '''
    class that makes a basic slider object
    '''
    def __init__(self, alignment, x, y, length, value1, value2, text):
        '''
        init takes in alignment which is a string saying whether it is horizontal or vertical
        the x and y are the x and y positions of the slider, the length is how long the slider
        is, the value1 and value2 are the min and max values that the slider should range
        between, and the text is the label that the slider should have
        '''
        self.__alignment = alignment
        self.__x = x
        self.__y = y
        self.__length = length
        self.__value1 = value1
        self.__value2 = value2
        self.__lineColor = (255,255,255)
        self.__knobColor = (231, 195, 80)
        self.__text = text
        if self.__alignment == 'horizontal':
            self.__knobx = self.__x + self.__length/2
            self.__knoby = self.__y
            self.__rect = Rect(self.__knobx - 5, self.__knoby - 10, 10, 20)
        if self.__alignment == 'vertical':
            self.__knobx = self.__x
            self.__knoby = self.__y + self.__length/2
            self.__rect = Rect(self.__knobx - 10, self.__knoby - 5, 20, 10)


    def draw(self, screen):
        '''
        this draws the slider to the screen (surface) passed to it
        '''
        #text setup (to print plus and minus signs next to slider and its label underneath)
        font = pygame.font.Font("freesansbold.ttf", 24)
        textSurface1 = font.render("-", True, (255,255,255))
        textSurface2 = font.render("+", True, (255,255,255))
        font2 = pygame.font.Font("freesansbold.ttf", 16)
        textSurface = font2.render(self.__text, True, (255,255,255))
        
        #drawing for horizontally aligned slider
        if self.__alignment == 'horizontal':
            #draw main line
            pygame.draw.line(screen, self.__lineColor, (self.__x, self.__y), (self.__x + self.__length, self.__y), 1)

            #draw endpoint lines
            pygame.draw.line(screen, self.__lineColor, (self.__x, self.__y + 5), (self.__x, self.__y - 5), 1)
            pygame.draw.line(screen, self.__lineColor, (self.__x + self.__length, self.__y + 5), (self.__x + self.__length, self.__y - 5), 1)

            #draw knob
            pygame.draw.rect(screen, self.__knobColor, self.__rect, 0)
            
            #print text
            plusRect = Rect(self.__x - 25, self.__y - 15, 25, 25)
            screen.blit(textSurface1, plusRect)
            minusRect = Rect(self.__x + self.__length + 10, self.__y - 15, 25, 25)
            screen.blit(textSurface2, minusRect)
            textRect = Rect(self.__x + self.__length/2 - 25, self.__y + 15, 50, 50)
            screen.blit(textSurface, textRect)
        
        #drawing for vertically aligned slider (text not implemented)
        if self.__alignment == 'vertical':
            #draw main line
            pygame.draw.line(screen, self.__lineColor, (self.__x, self.__y), (self.__x, self.__y + self.__length), 1)
            
            #draw endpoint lines
            pygame.draw.line(screen, self.__lineColor, (self.__x - 10, self.__y), (self.__x + 10, self.__y), 1)
            pygame.draw.line(screen, self.__lineColor, (self.__x - 10, self.__y + self.__length), (self.__x + 10, self.__y + self.__length), 1)
            
            #draw knob
            pygame.draw.rect(screen, self.__knobColor, self.__color, self.__rect, 0)

    def click(self, mouse):
        '''
        this changes the slider knob's location if the slider was clicked on; the mouse x
        and y position are passed and it checks to see if it is within range of the slider;
        if the mouse is on the slider, than the knob's location will be moved to where clicked
        '''
        if self.__alignment == 'horizontal':
            if mouse[0] >= self.__x and mouse[0] <= self.__x + self.__length and mouse[1] >= self.__y - 10 and mouse[1] <= self.__y + 10:
                self.__knobx = mouse[0]
                self.__rect = Rect(self.__knobx - 5, self.__knoby - 10, 10, 20)
        if self.__alignment == 'vertical':
            if mouse[0] >= self.__x - 10 and mouse[0] <= self.__x + 10 and mouse[1] >= self.__y and mouse[1] <= self.__y + self.__length:
                self.__knoby = mouse[1]
                self.__rect = Rect(self.__knobx - 10, self.__knoby - 5, 20, 10)


    def getValue(self):
        '''
        calculates the value according to the position of the knob and returns it;
        currently only the horizontal slider has been implemented due to time
        constraints and lack of need of a vertical slider
        '''
        if self.__alignment == 'horizontal':
            inputMax = self.__value2 - self.__value1
            return (self.__knobx - self.__x)*inputMax/self.__length + self.__value1

    def changeKnobColor(self, newColor):
        self.__knobColor = newColor

    def changeLineColor(self, newColor):
        self.__lineColor = newColor
