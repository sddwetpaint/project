import pygame
from pygame.locals import *

class Button:
    '''
    Default button class with a template click function that calls a non-implemented action() method
    '''
    def __init__(self, width, height, x, y, color, world, surface, text, textColor=(255,255,255), textSize=10):
        self._width = width
        self._height = height
        self._x = x
        self._y = y
        self._color = color
        self._rect = Rect(self._x, self._y, self._width, self._height)
        self._world = world
        self._surface = surface
        self._text = text
        self._textColor = textColor
        self._textSize = textSize

    def getWidth(self):
        return self._width

    def getHeight(self):
        return self._height

    def getX(self):
        return self._x

    def getY(self):
        return self._y

    def setX(self, x):
        self._x = x

    def setY(self, y):
        self._y = y

    def setColor(self, color):
        self._color = color

    def setText(self, text):
        self._text = text

    def draw(self, surface):
        '''
        Draws the rectangle background of button and then the text is 
        drawn in the center of that rectangle.
        '''
        pygame.draw.rect(surface, self._color, self._rect, 0)
        font = pygame.font.Font("freesansbold.ttf", self._textSize)
        textSurface = font.render(self._text, True, self._textColor)
        textRect = ((self._x + (self._width / 2)) - (textSurface.get_width() / 2), 
                    ((self._y + (self._height / 2)) - (textSurface.get_height() / 2)))
        surface.blit(textSurface, textRect)

    def click(self, mouse):      
        '''
        If the coordinates lie in the rectangular area of the button, 
        run the _action()
        '''
        if mouse[0] >= self._x and mouse[1] >= self._y and \
           mouse[0] <= (self._x + self._width) and mouse[1] <= (self._y + self._height):
            return self._action()

    def _action(self):
        '''
        This is the on-click function that occurs when the button is clicked. 
        It is meant to be overridden.
        '''
        raise NotImplementedError()


class ClickedButton(Button):
    '''
    Subclass of Button: Implements a simple button that returns true when "clicked"
    '''
    def __init__(self, width, height, x, y, color, world, surface, text, textColor=(255,255,255), textSize=10):
        Button.__init__(self, width, height, x, y, color, world, surface, text, textColor, textSize)

    def _action(self):
        return True