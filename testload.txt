{
    "genepool": {
        "shape": [
            [
                "triangle", 
                0.25, 
                1
            ], 
            [
                "square", 
                0.25, 
                2
            ], 
            [
                "circle", 
                0.25, 
                3
            ], 
            [
                "rounded square", 
                0.25, 
                4
            ]
        ], 
        "vision": [
            [
                25, 
                0.1, 
                0
            ], 
            [
                50, 
                0.35, 
                0
            ], 
            [
                75, 
                0.3, 
                0
            ], 
            [
                100, 
                0.25, 
                0
            ]
        ], 
        "socialness": [
            [
                1, 
                0.2, 
                0
            ], 
            [
                2, 
                0.4, 
                0
            ], 
            [
                3, 
                0.2, 
                0
            ], 
            [
                5, 
                0.1, 
                0
            ], 
            [
                7, 
                0.1, 
                0
            ]
        ], 
        "color": [
            [
                "red", 
                0.17, 
                3
            ], 
            [
                "blue", 
                0.17, 
                5
            ], 
            [
                "green", 
                0.17, 
                2
            ], 
            [
                "orange", 
                0.17, 
                1
            ], 
            [
                "yellow", 
                0.16, 
                6
            ], 
            [
                "purple", 
                0.16, 
                4
            ]
        ], 
        "attnspan": [
            [
                1, 
                0.2, 
                0
            ], 
            [
                30, 
                0.2, 
                0
            ], 
            [
                50, 
                0.4, 
                0
            ], 
            [
                100, 
                0.1, 
                0
            ], 
            [
                150, 
                0.1, 
                0
            ]
        ], 
        "fertility": [
            [
                0.0, 
                0.2, 
                0
            ], 
            [
                0.02, 
                0.3, 
                0
            ], 
            [
                0.06, 
                0.3, 
                0
            ], 
            [
                0.1, 
                0.2, 
                0
            ]
        ], 
        "speed": [
            [
                3, 
                0.125, 
                1
            ], 
            [
                4, 
                0.125, 
                4
            ], 
            [
                5, 
                0.125, 
                7
            ], 
            [
                6, 
                0.125, 
                8
            ], 
            [
                7, 
                0.125, 
                6
            ], 
            [
                8, 
                0.125, 
                5
            ], 
            [
                9, 
                0.125, 
                3
            ], 
            [
                10, 
                0.125, 
                2
            ]
        ], 
        "lifespan": [
            [
                500, 
                0.1, 
                0
            ], 
            [
                1000, 
                0.2, 
                0
            ], 
            [
                2000, 
                0.4, 
                0
            ], 
            [
                4000, 
                0.2, 
                0
            ], 
            [
                10000, 
                0.1, 
                0
            ]
        ], 
        "size": [
            [
                0.6, 
                0.15, 
                0
            ], 
            [
                0.9, 
                0.275, 
                0
            ], 
            [
                1.2, 
                0.375, 
                0
            ], 
            [
                1.4, 
                0.2, 
                0
            ]
        ]
    }, 
    "totalPop": 3, 
    "critters": {
        "googly eyes": {
            "direction": 0, 
            "name": "googly eyes", 
            "position": [
                346, 
                217
            ], 
            "gender": "female", 
            "age": 0, 
            "genotype": {
                "color": [
                    [
                        "blue", 
                        0.17, 
                        5
                    ], 
                    [
                        "blue", 
                        0.17, 
                        5
                    ]
                ], 
                "lifespan": [
                    [
                        4000, 
                        0.2, 
                        0
                    ], 
                    [
                        2000, 
                        0.4, 
                        0
                    ]
                ], 
                "shape": [
                    [
                        "circle", 
                        0.25, 
                        3
                    ], 
                    [
                        "circle", 
                        0.25, 
                        3
                    ]
                ], 
                "socialness": [
                    [
                        3, 
                        0.2, 
                        0
                    ], 
                    [
                        2, 
                        0.4, 
                        0
                    ]
                ], 
                "attnspan": [
                    [
                        1, 
                        0.2, 
                        0
                    ], 
                    [
                        50, 
                        0.4, 
                        0
                    ]
                ], 
                "fertility": [
                    [
                        0.06, 
                        0.3, 
                        0
                    ], 
                    [
                        0.06, 
                        0.3, 
                        0
                    ]
                ], 
                "speed": [
                    [
                        9, 
                        0.125, 
                        3
                    ], 
                    [
                        4, 
                        0.125, 
                        4
                    ]
                ], 
                "vision": [
                    [
                        50, 
                        0.35, 
                        0
                    ], 
                    [
                        100, 
                        0.25, 
                        0
                    ]
                ], 
                "size": [
                    [
                        1.2, 
                        0.375, 
                        0
                    ], 
                    [
                        1.4, 
                        0.2, 
                        0
                    ]
                ]
            }, 
            "attnspan": 25, 
            "phenotype": {
                "color": "blue", 
                "lifespan": 3000, 
                "shape": "circle", 
                "socialness": 2, 
                "attnspan": 25, 
                "fertility": 0.06, 
                "speed": 4, 
                "vision": 75, 
                "size": 1.2999999999999998
            }
        }, 
        "antidepressants": {
            "direction": 0, 
            "name": "antidepressants", 
            "position": [
                638, 
                209
            ], 
            "gender": "male", 
            "age": 0, 
            "genotype": {
                "color": [
                    [
                        "purple", 
                        0.16, 
                        4
                    ], 
                    [
                        "purple", 
                        0.16, 
                        4
                    ]
                ], 
                "lifespan": [
                    [
                        4000, 
                        0.2, 
                        0
                    ], 
                    [
                        2000, 
                        0.4, 
                        0
                    ]
                ], 
                "shape": [
                    [
                        "rounded square", 
                        0.25, 
                        4
                    ], 
                    [
                        "rounded square", 
                        0.25, 
                        4
                    ]
                ], 
                "socialness": [
                    [
                        3, 
                        0.2, 
                        0
                    ], 
                    [
                        3, 
                        0.2, 
                        0
                    ]
                ], 
                "attnspan": [
                    [
                        1, 
                        0.2, 
                        0
                    ], 
                    [
                        100, 
                        0.1, 
                        0
                    ]
                ], 
                "fertility": [
                    [
                        0.06, 
                        0.3, 
                        0
                    ], 
                    [
                        0.02, 
                        0.3, 
                        0
                    ]
                ], 
                "speed": [
                    [
                        5, 
                        0.125, 
                        7
                    ], 
                    [
                        6, 
                        0.125, 
                        8
                    ]
                ], 
                "vision": [
                    [
                        75, 
                        0.3, 
                        0
                    ], 
                    [
                        50, 
                        0.35, 
                        0
                    ]
                ], 
                "size": [
                    [
                        1.4, 
                        0.2, 
                        0
                    ], 
                    [
                        0.6, 
                        0.15, 
                        0
                    ]
                ]
            }, 
            "attnspan": 50, 
            "phenotype": {
                "color": "purple", 
                "lifespan": 3000, 
                "shape": "rounded square", 
                "socialness": 3, 
                "attnspan": 50, 
                "fertility": 0.04, 
                "speed": 6, 
                "vision": 62, 
                "size": 1.0
            }
        }, 
        "are the best": {
            "direction": 0, 
            "name": "are the best", 
            "position": [
                462, 
                529
            ], 
            "gender": "male", 
            "age": 0, 
            "genotype": {
                "color": [
                    [
                        "purple", 
                        0.16, 
                        4
                    ], 
                    [
                        "purple", 
                        0.16, 
                        4
                    ]
                ], 
                "lifespan": [
                    [
                        4000, 
                        0.2, 
                        0
                    ], 
                    [
                        2000, 
                        0.4, 
                        0
                    ]
                ], 
                "shape": [
                    [
                        "rounded square", 
                        0.25, 
                        4
                    ], 
                    [
                        "rounded square", 
                        0.25, 
                        4
                    ]
                ], 
                "socialness": [
                    [
                        3, 
                        0.2, 
                        0
                    ], 
                    [
                        3, 
                        0.2, 
                        0
                    ]
                ], 
                "attnspan": [
                    [
                        1, 
                        0.2, 
                        0
                    ], 
                    [
                        100, 
                        0.1, 
                        0
                    ]
                ], 
                "fertility": [
                    [
                        0.06, 
                        0.3, 
                        0
                    ], 
                    [
                        0.02, 
                        0.3, 
                        0
                    ]
                ], 
                "speed": [
                    [
                        5, 
                        0.125, 
                        7
                    ], 
                    [
                        6, 
                        0.125, 
                        8
                    ]
                ], 
                "vision": [
                    [
                        75, 
                        0.3, 
                        0
                    ], 
                    [
                        50, 
                        0.35, 
                        0
                    ]
                ], 
                "size": [
                    [
                        1.4, 
                        0.2, 
                        0
                    ], 
                    [
                        0.6, 
                        0.15, 
                        0
                    ]
                ]
            }, 
            "attnspan": 50, 
            "phenotype": {
                "color": "purple", 
                "lifespan": 3000, 
                "shape": "rounded square", 
                "socialness": 3, 
                "attnspan": 50, 
                "fertility": 0.04, 
                "speed": 6, 
                "vision": 62, 
                "size": 1.0
            }
        }
    }, 
    "livingPop": 3
}