worldX = 998
worldY = 600
worldX0 = 0
worldY0 = 22
worldExists = False
maxPopDensity = 4

# RGB values of critter colors
colors = {'red': (255, 161, 149),
          'orange': (253, 189, 118),
          'yellow': (248, 233, 110),
          'green': (149, 215, 138),
          'blue': (134, 190, 199),
          'purple': (215, 153, 240)}

# given in format (geneName, base frequency, dominance)
# If dominance value is 0, it means codominance
defaultGenepool = {
                    "color": 
                        [("red", 0.17, 3), 
                        ("blue", 0.17, 5), 
                        ("green", 0.17, 2),
                        ('orange', 0.17, 1), 
                        ('yellow', 0.16, 6), 
                        ('purple', 0.16, 4)],
                    'shape': 
                        [('triangle', 0.25, 1), 
                        ('square', 0.25, 2),
                        ('circle', 0.25, 3), 
                        ('rounded square', 0.25, 4)],
                    'speed': 
                        [(3, 0.125, 1), 
                        (4, 0.125, 4), 
                        (5, 0.125, 7), 
                        (6, 0.125, 8),
                        (7, 0.125, 6), 
                        (8, 0.125, 5), 
                        (9, 0.125, 3), 
                        (10, 0.125, 2)],
                    'size': 
                        [(0.6, 0.15, 0), 
                        (0.9, 0.275, 0), 
                        (1.2, 0.375, 0), 
                        (1.4, 0.2, 0)],
                    'lifespan': 
                        [(500, 0.1, 0), 
                        (1000, 0.2, 0), 
                        (2000, 0.4, 0),
                        (4000, 0.2, 0), 
                        (10000, 0.1, 0)],
                    'fertility': 
                        [(0.0, 0.2, 0), 
                        (0.02, 0.3, 0), 
                        (0.06, 0.3, 0), 
                        (0.1, 0.2, 0)],
                    'attnspan': 
                        [(1, 0.2, 0), 
                        (30, 0.2, 0), 
                        (50, 0.4, 0), 
                        (100, 0.1, 0), 
                        (150, 0.1, 0)],
                    'socialness': 
                        [(1, 0.2, 0), 
                        (2, 0.4, 0), 
                        (3, 0.2, 0), 
                        (5, 0.1, 0),
                        (7, 0.1, 0)],
                    'vision': 
                        [(25, 0.1, 0), 
                        (50, 0.35, 0), 
                        (75, 0.3, 0), 
                        (100, 0.25, 0)]
                }