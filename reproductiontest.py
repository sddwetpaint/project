import unittest
import math
from genome import Genome
from critter import Critter
from world import World

class TestReproduction(unittest.TestCase):
    def setUp(self):
        self.world = World()
        self.babies = []

        g0 = self.world.generateGenome()
        self.darwin = Critter('Darwin', g0)
        self.world.addCritter(self.darwin)
        self.darwin.gender = 'male'

        g1 = self.world.generateGenome()
        self.galton = Critter('Galton', g1)
        self.galton.phenotypes['size'] = self.darwin.phenotypes['size']
        self.world.addCritter(self.galton)
        self.galton.gender = 'female'

    def sameGene(self, gene1, gene2):
        return (gene1.name == gene2.name and
                gene1.value == gene2.value and
                gene1.frequency == gene2.frequency and
                gene1.dominance == gene2.dominance)

    def testGeneticInheritance(self):
        darwinGenes = self.darwin.genotype.genomeDict
        galtonGenes = self.galton.genotype.genomeDict
        for i in range (0, 10):
            g = Genome(self.darwin.genotype, self.galton.genotype).genomeDict
            for geneType in g:
                # check for all valid gene combinations
                self.assertTrue((((self.sameGene(g[geneType][0], darwinGenes[geneType][0]) or
                    self.sameGene(g[geneType][0], darwinGenes[geneType][1])) and
                    (self.sameGene(g[geneType][1], galtonGenes[geneType][0]) or 
                    self.sameGene(g[geneType][1], galtonGenes[geneType][1])))))
        print "genetic inheritance pass"

    def testReproductionRate(self):
        testLengths = [10, 100, 200]
        for testLength in testLengths:
            initPop = self.world.totalPopulation
            babies = []

            # computing confidence interval to give joint confidence interval of 95%
            # instead of just doing +/-5% (because statistics just doesn't work that way)
            rate = self.galton.phenotypes["fertility"]
            mean = testLength * rate
            zScore = 2.39
            errorBound = zScore * math.sqrt(rate * (1.0 - rate) / testLength)
            #print "errorBound:", errorBound

            for i in range(0, testLength):
                self.darwin.x = 100
                self.darwin.y = 100
                self.galton.x = 100
                self.galton.y = 100
                self.world.moveCritters()
                babies = [name for name in self.world.critters.keys() if name != "Darwin" and name != "Galton"]
                for name in babies:
                    self.world.killCritter(name)
            #print "counts:", self.world.totalPopulation, initPop
            curPop = self.world.totalPopulation - initPop

            minVal = math.floor(mean - errorBound)
            maxVal = math.ceil(mean + errorBound)
            #print "expected:", minVal, "to", maxVal, "averaging", mean
            #print "found:", curPop
            self.assertTrue(curPop <= maxVal)
            self.assertTrue(curPop >= minVal)
            #print "n =", testLength, "complete"
            #print
        print "reproduction rate pass"

    def testCollisions(self):
        testLengths = [10, 100, 200]
        for testLength in testLengths:
            initPop = self.world.totalPopulation
            babies = []

            for i in range(0, testLength):
                self.darwin.x = 100
                self.darwin.y = 100
                self.galton.x = 100
                self.galton.y = 101 + 15 * self.galton.phenotypes["size"]
                self.world.moveCritters()
                babies = [name for name in self.world.critters.keys() if name != "Darwin" and name != "Galton"]
                for name in babies:
                    self.assertTrue(self.world.getCloseCritters(self.world.critters[name], 0) == [])
                    self.world.killCritter(name)
        print "baby collision pass"

                    
if __name__ == "__main__":
    unittest.main()