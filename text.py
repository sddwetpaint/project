import pygame
from pygame.locals import *


class JustText:
    '''
    Creates a text block that can be drawn
    '''
    def __init__(self, width, height, x, y, color, text, textColor=(255,255,255), textSize=10):
        self._width = width
        self._height = height
        self._x = x
        self._y = y
        self._color = color
        self._rect = Rect(self._x, self._y, self._width, self._height)
        self._textColor = textColor
        self._textSize = textSize
        self._text = text

    def getWidth(self):
        return self._width

    def getHeight(self):
        return self._height

    def getX(self):
        return self._x

    def getY(self):
        return self._y

    def setX(self, x):
        self._x = x

    def setY(self, y):
        self._y = y

    def setColor(self, color):
        self._color = color

    def setText(self, text):
        self._text = text

    def draw(self, surface):
        '''
        Draws the rectangle background, then the text line by line on top. Text is left alligned.
        '''
        pygame.draw.rect(surface, self._color, self._rect, 0)

        lines = self._text.splitlines()
        dy = 5

        for line in lines:
            font = pygame.font.Font("freesansbold.ttf", self._textSize)
            textSurface = font.render(line, True, self._textColor)
            textRect = ((self._x + 10), (self._y + dy))
            dy += font.get_linesize()

            surface.blit(textSurface, textRect)
