import pygame
from pygame.locals import *
from fileButton import FileButton
from window import Window

'''
Includes:
	- NewFileButton
	- OpenFileButton
	- SaveFileButton
'''

class NewFileButton(FileButton):
	'''
	Subclass of Button: Implements a "New World/File" button.
	'''
	def __init__(self, width, height, x, y, color, world, surface, text, textColor=(255,255,255), textSize=10):
		FileButton.__init__(self, width, height, x, y, color, world, surface, text, textColor, textSize)

	def _action(self):
		'''
		Creates a new world after a prompt to save and create, don't save and create, or cancel
		''' 
		rc, fileloc = self._prompt("Save & New", "New")
		if rc == 0:
			self._world.saveWorld()
		if rc < 2:
			self._world.fileloc = fileloc
			self._world.clearWorld()

class OpenFileButton(FileButton):
	'''
	Subclass of Button: Implements a "Open File" button.
	'''
	def __init__(self, width, height, x, y, color, world, surface, text, textColor=(255,255,255), textSize=10):
		FileButton.__init__(self, width, height, x, y, color, world, surface, text, textColor, textSize)

	def _action(self):
		'''
		Opens saved world after a prompt to save and open, don't save and open, or cancel
		''' 
		rc, fileloc = self._prompt("Save & Open", "Don't Save & Open")
		if rc == 0:
			self._world.saveWorld()
		if rc <= 1:
			self._world.fileloc = fileloc
			exists = self._world.loadWorld()
			if not exists:
				self._error("File <" + fileloc + "> does not exist.");


class SaveFileButton(FileButton):
	'''
	Subclass of FileButton: Implements a "Save File" button.
	'''
	def __init__(self, width, height, x, y, color, world, surface, text, textColor=(255,255,255), textSize=10):
		FileButton.__init__(self, width, height, x, y, color, world, surface, text, textColor, textSize)

	def _action(self):
		'''
		Saves world to file after a prompt to save or cancel
		''' 
		rc, fileloc = self._prompt("Save")
		if rc == 0:
			self._world.fileloc = fileloc
			self._world.saveWorld()

