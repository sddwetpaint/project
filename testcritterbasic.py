import unittest
from genome import Genome
from critter import Critter
from world import World


class TestCritter(unittest.TestCase):
    def setUp(self):
        self.world = World()
        self.critter = []
        
        g0 = self.world.generateGenome()
        self.critter.append(Critter('what is my purpose', g0))
        self.world.addCritter(self.critter[0])
        self.assertEqual(self.world.totalPopulation, 1)

        g1 = self.world.generateGenome()
        self.critter.append(Critter('you pass butter', g1))
        self.world.addCritter(self.critter[1])
        self.assertEqual(self.world.totalPopulation, 2)

        g2 = self.world.generateGenome()
        self.critter.append(Critter('oh my god', g2))
        self.world.addCritter(self.critter[2])
        self.assertEqual(self.world.totalPopulation, 3)


    def testAddCritter(self):
        print 'Test CritterADD: adding critter \'just a test\'...'
        g2 = self.world.generateGenome()
        self.critter.append(Critter('just a test', g2))
        self.world.addCritter(self.critter[3])
        self.assertEqual(self.world.totalPopulation, 4)
        print 'passed'

    def testCritterStats(self):
        print 'Test CritterSTATS: parsing and comparing critter information of \'what is my purpose\'...'
        phenotypes = self.critter[0].phenotypes
        compare = self.world.getCritter(self.critter[0].name)
        critraw = [s.strip().split(':') for s in compare.splitlines()]

        critdata = {}
        for item in critraw:
            key = item[0]
            val = item[1].strip()
            if key == 'Name' or key == 'gender' or key == 'age':
                self.assertEqual(val, str(getattr(self.critter[0], key.lower())))
            else:
                if key == 'color' or key == 'shape':
                    val = val[:val.index('(')].strip()
                critdata[key] = val

        for key, val in critdata.iteritems():
            self.assertEqual(val, str(phenotypes[key]))
        print 'passed'

    def testSelectCritter(self):
        x = self.critter[0].x
        y = self.critter[0].y
        size = int(15 * self.critter[0].phenotypes['size'])/2

        sizer = int(15 * self.critter[1].phenotypes['size'])/2
        self.critter[1].x = x + sizer + size + 1
        self.critter[1].y = y

        sized = int(15 * self.critter[2].phenotypes['size'])/2
        self.critter[2].x = x
        self.critter[2].y = y + sized + size + 1

        print 'Test CritterSELECT: selecting critter \'what is my purpose\'...'
        # normal select
        self.world.selectCritter((x, y))
        self.assertEqual(self.critter[0].selected, True)
        self.assertEqual(self.critter[1].selected, False)
        self.assertEqual(self.critter[2].selected, False)
        
        # outside bounding box
        self.world.selectCritter((x+40, y+40))
        self.assertEqual(self.critter[0].selected, False)
        self.assertEqual(self.critter[1].selected, False)
        self.assertEqual(self.critter[2].selected, False)

        # just outside the top
        self.world.selectCritter((x, y-size-1))
        self.assertEqual(self.critter[0].selected, False)
        self.assertEqual(self.critter[1].selected, False)
        self.assertEqual(self.critter[2].selected, False)

        # just inside top left corner
        self.world.selectCritter((x-size+1, y-size+1))
        self.assertEqual(self.critter[0].selected, True)
        self.assertEqual(self.critter[1].selected, False)
        self.assertEqual(self.critter[2].selected, False)

        # just out side top left corner
        self.world.selectCritter((x-size, y-size))
        self.assertEqual(self.critter[0].selected, False)
        self.assertEqual(self.critter[1].selected, False)
        self.assertEqual(self.critter[2].selected, False)

        print '    selecting critter \'you pass butter\'...'
        # between critter 0 and 1, just inside 1
        self.world.selectCritter((x+size+2, y))
        self.assertEqual(self.critter[0].selected, False)
        self.assertEqual(self.critter[1].selected, True)
        self.assertEqual(self.critter[2].selected, False)

        print '    selecting critter \'oh my god\'...'
        # between critter 0 and 2, just inside 2
        self.world.selectCritter((x, y+size+2))
        self.assertEqual(self.critter[0].selected, False)
        self.assertEqual(self.critter[1].selected, False)
        self.assertEqual(self.critter[2].selected, True)
        print 'passed'
        print "--------------- CRITTERS CLEAR I REPEAT CRITTERS CLEAR ---------------"


    def testKillCritter(self):
        print 'Test CritterKILL: killing critter \'oh my god\''
        self.world.killCritter('oh my god')
        self.assertEqual(self.world.livingPopulation, 2)
        print 'passed'


if __name__ == "__main__":
    unittest.main()