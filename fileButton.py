import pygame
import config
import string
from pygame.locals import *
from button import Button, ClickedButton
from world import World
from window import Window
from critter import Critter

class FileButton(Button):
    '''
    Abstract Subclass of Button
    Additional private variable and functions related to filename
    '''
    def __init__(self, width, height, x, y, color, world, surface, text, textColor=(255,255,255), textSize=10):
        Button.__init__(self, width, height, x, y, color, world, surface, text, textColor, textSize)

    def _error(self, msg):
        '''
        Creates an error prompt that the user has to acknowledge to continue.
        '''
        pygame.draw.rect(self._surface.getDisplay(), self._color, (430,250,325,50), 0) 
        pygame.font.init()
        font = pygame.font.Font("freesansbold.ttf", 16)
        textSurface = font.render(msg, True, self._textColor)
        self._surface.getDisplay().blit(textSurface, (435,257))
        okbtn = ClickedButton(315, 15, 435, 280, (255,255,255), self._world, self._surface, 'Close', (0,0,0))
        okbtn.draw(self._surface.getDisplay())
        pygame.display.flip()

        confirmed = False
        while not confirmed:
            event = pygame.event.poll()
            if event.type == MOUSEBUTTONDOWN and okbtn.click(pygame.mouse.get_pos()):
                confirmed = True

    def _prompt(self, promptText, quitText=None):
        '''
        Creates an input prompt for user to input file. 
        Referenced from http://www.pygame.org/pcr/inputbox/ for input
        '''
        def get_key():
            while True:
                event = pygame.event.poll()
                if event.type == KEYDOWN:
                    return event.key
                elif event.type == MOUSEBUTTONDOWN:
                    return None
                else:
                    pass

        def inputbox(surface, fileloc):
            fontobject = pygame.font.Font("freesansbold.ttf",12)
            pygame.draw.rect(self._surface.getDisplay(), (255,255,255), (485,255,265,20), 0)
            if len(fileloc) != 0:
                self._surface.getDisplay().blit(fontobject.render(fileloc, 1, (0,0,0)), (490,260))
            pygame.display.flip()

        # Making the input box for file
        fileloc = list(self._world.fileloc)
        pygame.draw.rect(self._surface.getDisplay(), self._color, (430,250,325,50), 0)
        pygame.font.init()
        font = pygame.font.Font("freesansbold.ttf", 16)
        textSurface = font.render("File: ", True, self._textColor)
        self._surface.getDisplay().blit(textSurface, (435, 257))
        inputbox(self._surface, string.join(fileloc,""))

        # Making the buttons for prompt
        buttons = []
        if (quitText != None):
            savebtn = ClickedButton(100, 15, 435, 280, (255,255,255), self._world, self._surface, promptText, (0,0,0))
            savebtn.draw(self._surface.getDisplay())
            buttons.append(savebtn)
            quitbtn = ClickedButton(110, 15, 540, 280, (255,255,255), self._world, self._surface, quitText, (0,0,0))
            quitbtn.draw(self._surface.getDisplay())
            buttons.append(quitbtn)
            cancelbtn = ClickedButton(95, 15, 655, 280, (255,255,255), self._world, self._surface, "Cancel", (0,0,0))
        else:
            savebtn = ClickedButton(155, 15, 435, 280, (255,255,255), self._world, self._surface, promptText, (0,0,0))
            savebtn.draw(self._surface.getDisplay())
            buttons.append(savebtn)
            cancelbtn = ClickedButton(155, 15, 595, 280, (255,255,255), self._world, self._surface, "Cancel", (0,0,0))
        cancelbtn.draw(self._surface.getDisplay())
        buttons.append(cancelbtn)
        pygame.display.flip()

        # Get user input. If user clicks, check for which button he clicked on.
        while True:
            inkey = get_key()
            if inkey == None:
                for i in range(0, len(buttons)):
                    if buttons[i].click(pygame.mouse.get_pos()):
                        return i, string.join(fileloc, "")
            elif inkey == K_BACKSPACE:
                fileloc = fileloc[0:-1]
            elif inkey == K_RETURN:
                return 0, string.join(fileloc, "")
            elif inkey == K_MINUS:
                fileloc.append("_")
            elif inkey <= 127:
                fileloc.append(chr(inkey))
            inputbox(self._surface, string.join(fileloc,""))