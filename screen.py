import sys, pygame
from pygame.locals import *
from window import Window
from world import World
from critter import Critter

class Screen(object):
    '''
    class that will contain the Screen that controls what is currently being 
    displayed to the player
    '''
    def __init__(self, width, height):
        self.__width = width
        self.__height = height
        self.__surface = pygame.display.set_mode((self.__width, self.__height))
        self.__color = (0,0,0)
        self.__items = []

    def close(self):
        '''
        wasn't sure what to do for this, this is currently another method for 
        quitting pygame
        '''
        pygame.quit()
        sys.exit()

    def draw(self):
        '''
        draws the main surface of the game and draws all the items that are 
        currently contianed in self.__items onto it
        '''
        self.__surface.fill(self.__color)
        for item in self.__items:
            item.draw(self.__surface)
        #pygame.display.update()

    def getWidth(self):
        return self.__width

    def getHeight(self):
        return self.__height

    def getColor(self):
        return self.__color

    def getDisplay(self):
        return self.__surface

    def addItem(self, newItem):
        '''
        adds an item to self.__items (adding this item means it will be drawn
        to the screen); items should be added in correct order; the first item
        will be drawn first and everything following it will be drawn on top
        '''

        #checks to see if the item has already been added
        for item in self.__items:
            if item == newItem:
                print 'ERROR: an item named ' + newItem + ' has already been added to the surface'
                return

        self.__items.append(newItem)

    def deleteItem(self, itemName):
        self.__items.remove(itemName)

    def changeColor(self, color):
        self.__color = color

