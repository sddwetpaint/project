import unittest
import json
import config
from genome import Genome
from critter import Critter
from world import World


class TestWorld(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None
        self.world = World()
        self.world.fileloc = 'test.txt'

        self.critter = []
        
        g0 = self.world.generateGenome()
        self.critter.append(Critter('what is my purpose', g0))
        self.world.addCritter(self.critter[0])
        self.assertEqual(self.world.totalPopulation, 1)

        g1 = self.world.generateGenome()
        self.critter.append(Critter('you pass butter', g1))
        self.world.addCritter(self.critter[1])
        self.assertEqual(self.world.totalPopulation, 2)

        g2 = self.world.generateGenome()
        self.critter.append(Critter('oh my god', g1))
        self.world.addCritter(self.critter[2])
        self.assertEqual(self.world.totalPopulation, 3)

        self.preload = {}
        with open('testload.txt', 'r') as content_file:
            self.preload = json.loads(content_file.read())


    def testNewWorld(self):
        print 'Test WorldNEW: creating new world...'
        claudia = World()
        self.assertEqual(claudia.genepool, config.defaultGenepool)
        self.assertEqual(claudia.totalPopulation, 0)
        self.assertEqual(claudia.livingPopulation, 0)
        self.assertEqual(claudia.fileloc, 'untitled')
        self.assertEqual(claudia.critters, {})
        print 'passed'

    def testSaveWorld(self):
        print 'Test WorldSAVE: saving world...'
        self.world.saveWorld()
       
        allData = {}
        with open(self.world.fileloc, 'r') as content_file:
            allData = json.loads(content_file.read())

        print '    checking genepool and population...'
        self.assertEqual(self.world.livingPopulation, allData['livingPop'])
        self.assertEqual(self.world.totalPopulation, allData['totalPop'])
        
        self.halp(self.world.genepool, allData['genepool'])
        
        print '    checking critters...'
        self.halp(self.critter[0].saveItself(), allData['critters'][self.critter[0].name])
        self.halp(self.critter[1].saveItself(), allData['critters'][self.critter[1].name])
        self.halp(self.critter[2].saveItself(), allData['critters'][self.critter[2].name])
        print 'passed'
        print "------------------ WORLD CLEAR I REPEAT WORLD CLEAR ------------------"

    def testLoadWorld(self):
        print 'Test WorldLOAD: loading world from preset file...'
        self.world.fileloc = 'testload.txt'
        self.world.loadWorld()

        print '    checking predetermined world info and critters...'
        self.assertEqual(self.world.livingPopulation, self.preload['livingPop'])
        self.assertEqual(self.world.totalPopulation, self.preload['totalPop'])
        
        self.assertEqual(self.world.genepool, self.preload['genepool'])

        self.halp(self.world.critters['googly eyes'].saveItself(), 
                    self.preload['critters']['googly eyes'])
        self.halp(self.world.critters['are the best'].saveItself(), 
                    self.preload['critters']['are the best'])
        self.halp(self.world.critters['antidepressants'].saveItself(), 
                    self.preload['critters']['antidepressants'])
        print 'passed'

    def testClearWorld(self):
        print 'Test WorldCLEAR: making sure clear world resets fields to default values...'
        self.world.clearWorld()
        self.assertEqual(self.world.genepool, config.defaultGenepool)
        self.assertEqual(self.world.totalPopulation, 0)
        self.assertEqual(self.world.livingPopulation, 0)
        self.assertEqual(self.world.fileloc, 'untitled')
        self.assertEqual(self.world.critters, {})
        print 'passed'

    def halp(self, dict1, dict2):
        for key, val in dict2.iteritems():
            if type(val) is list:
                for i in range(len(val)):
                    if hasattr(val[i], '__iter__'):
                        self.assertEqual(dict1[str(key)][i], tuple(val[i]))
                    else:
                        self.assertEqual(dict1[str(key)][i], val[i])
                        
            elif type(val) is dict:
                self.halp(dict1[str(key)], val)
            else:
                self.assertEqual(dict1[str(key)], val)


if __name__ == "__main__":
    unittest.main()