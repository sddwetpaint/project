import sys
import math
import random
import json
import config
import os.path
import re
from sets import Set
from critter import Critter
from genome import Genome
from gene import Gene
from pygame import *


# ensures that only one world exists at a time, maintaining its status as singleton
worldExists = False
class SingletonReinstantiationError(Exception):
    '''
    raised when attempting to instantiate a singleton class that already has something
    instantiated
    '''
    pass


class World:
    '''
    Class containing the location of all the critters, the genepool, and other global backend things
    '''

    def __init__(self, genepool = config.defaultGenepool):
        '''
        initialize world with edited genepool and name generation
        '''
        # maintain singleton status
        if config.worldExists:
            raise SingletonReinstantiationError
        worldExists = True

        self.genepool = genepool
        self.critters = {}
        self.newCritters = []
        self.deadCritters = []
        self.livingPopulation = 0
        self.totalPopulation = 0
        self.fileloc = "untitled"
        
        # this is a flag that indicates if it needs to check for
        # existence when generating names; it is set to true when
        # a list is exhausted, and set to false when new world
        # is created
        self.nameFlag = False

        # shuffle names in the files
        self.jumbleNames('male.txt')
        self.jumbleNames('female.txt')

        # initialize name generators
        self.maleNameGenerator = self.getValidName('male')
        self.femaleNameGenerator = self.getValidName('female')


    def __str__(self):
        '''
        returns each critter name, age, and coordinates, with each critter on a separate line
        '''
        ans = ''
        for critterName in self.critters:
            critter = self.critters[critterName]
            ans += (critterName + ' ' +
                   str(critter.gender) + ' ' +
                   str(critter.age) + ' ' +
                   str((critter.x, critter.y)) + ' ' +
                   str(critter.direction + '\n'))
        ans += '\n'
        return ans

    def getGenepool(self, attribute):
        '''
        given an attribute name, returns list of valid types for that attribute
        i.e. getGenepool('color') might return ['red', 'blue', 'yellow', 'green']
        '''
        attrDict = self.genepool[attribute]
        attrList = []
        for gene in attrDict:
            attrList.append(gene[0])
        return attrList

    def generateGenome(self):
        '''
        generates a random genome based on the rules outlined in the current genome
        '''
        genome = {}
        
        for geneType in self.genepool:
            # reset random selection procedure
            gene1 = None
            gene2 = None
            r1 = random.random()
            r2 = random.random()
            
            # select two genes for current geneType
            for gene in self.genepool[geneType]:
                # decrease random numbers by probability of current gene being selected
                r1 -= gene[1]
                r2 -= gene[1]
                # select first gene resulting in random number being negative
                if gene1 is None and r1 <= 0:
                    gene1 = Gene(geneType, gene[0], gene[1], gene[2])
                if gene2 is None and r2 <= 0:
                    gene2 = Gene(geneType, gene[0], gene[1], gene[2])
            # add genes to genome
            genome[geneType] = (gene1, gene2)
        
        finalGenome = Genome()
        finalGenome.genomeDict = genome
        return finalGenome

    def getCloseCritters(self, centerCritter, radius):
        '''
        returns a list of critters within a certain radius of the given critter
        '''
        #centerCritter = self.critters[name]
        closeCritters = []
        radius += int(15 * centerCritter.phenotypes['size'] / 2)
        
        for critterName in self.critters:
            critter = self.critters[critterName]
            curRad = radius + int(15 * critter.phenotypes['size'] / 2)
            dist = math.sqrt((critter.x - centerCritter.x) ** 2 + (critter.y - centerCritter.y) ** 2)
            
            if dist < curRad and critter != centerCritter:
                closeCritters.append(critter)
        
        return closeCritters

    def getCritterClusterDirection(self, critters, name = None):
        '''
        returns the direction (in radians, between pi and -pi) of the average location of the given
        critters
        returns angle to center of world if no critters passed but a name is given
        returns a random angle if no critters passed and no name is given
        '''
        thisCritter = self.critters[name]

        # if (no critters and have name) or (on edge of screen)
        if ((len(critters) == 0 and name is not None) or
           thisCritter.x - int(15 * thisCritter.phenotypes['size'] / 2) <= config.worldX0 or
           thisCritter.x + int(15 * thisCritter.phenotypes['size'] / 2) >= config.worldX or
           thisCritter.y - int(15 * thisCritter.phenotypes['size'] / 2) <= config.worldY0 or
           thisCritter.y + int(15 * thisCritter.phenotypes['size'] / 2) >= config.worldY):
            critter = self.critters[name]
            # move to center
            x = int((config.worldX - config.worldX0) / 2 - critter.x)
            y = int((config.worldY - config.worldY0) / 2 - critter.y)
            return math.atan2(y, x)
        
        # if (no critters and not have name)
        elif len(critters) == 0 and name is not None:
            # move in a random direction
            return random.uniform(-math.pi, math.pi)
        
        # otherwise, compute average of locations and return angle towards it
        else:
            xSum = 0
            ySum = 0
            for critter in critters:
                xSum += critter.x
                ySum += critter.y
            x = xSum / len(critters) - thisCritter.x
            y = ySum / len(critters) - thisCritter.y
            d = math.atan2(y, x)
            if d < 0:
                d += 2 * math.pi
            return d

    def addCritter(self, newCritter):
        '''
        adds a new critter to the population
        '''
        self.livingPopulation += 1
        self.totalPopulation += 1
        self.critters[newCritter.name] = newCritter

    def killCritter(self, name):
        '''
        remove a critter from the population
        '''
        self.critters.pop(name)
        self.livingPopulation -= 1

    def selectCritter(self, position):
        '''
        returns the name of the critter that is selected
        '''
        gotIt = None
        for critterName in self.critters:
            critter = self.critters[critterName]
            size = int(15 * critter.phenotypes['size'])/2
            if (position[0] < (critter.x + size) and position[0] > (critter.x - size) and
                position[1] < (critter.y + size) and position[1] > (critter.y - size)):
                critter.selected = True
                gotIt = critterName

        for crit in self.critters:
            if crit != gotIt:
                self.critters[crit].selected = False
        return gotIt

    def jumbleNames(self, textfile):
        '''
        Shuffles lines in textfile
        '''
        everything = ''
        jumble = []

        with open(textfile, 'r') as f:
            everything = f.read()

        everything = re.sub('[\s]', ' ', everything)

        jumble = everything.split()
        random.shuffle(jumble)
        everything = '\n'.join(jumble)

        with open(textfile, 'w') as o:
            o.write(everything)

    def getValidName(self, gender):
        '''
        Reads from a shuffled text file of names of the indicated gender and
        yield the next name when needed; dead critters will not free up names
        as it is seen as bad fortune to name one's offspring with the name of
        a deceased among the critters.

        There is no upper limit; when the file is exhausted the program simply
        reinitializes the generator and start looking for valid names from the
        beginning hoping someone died in between.

        I realize this contradicts the critter culture, but those are dire times.
        '''
        source = gender + '.txt'
        with open(source, 'r+') as f:
            for line in f:
                name = line.strip('\n')
                if self.nameFlag:
                    for critterName in self.critters:
                        if name == critterName:
                            continue
                yield name

    def loadWorld(self):
        '''
        Load world from encoded json string in fileLoc, a string ending with .txt
        If the file does not exist, returns False. Otherwise, it returns True.
        '''
        if not os.path.isfile(self.fileloc):
            return False
        
        allData = {}
        
        with open(self.fileloc, 'r') as content_file:
            allData = json.loads(content_file.read())

        self.genepool = allData['genepool']
        self.critters = {}

        for n, c in allData['critters'].iteritems():
            genedict = {}
            for genetype, genes in c['genotype'].iteritems():
                gene1 = Gene(genetype, genes[0][0], genes[0][1], genes[0][2])
                gene2 = Gene(genetype, genes[1][0], genes[1][1], genes[1][2])
                genedict[genetype] = (gene1, gene2)

            genome = Genome()
            genome.genomeDict = genedict
            crit = Critter(n, genome)
            crit.x = c['position'][0]
            crit.y = c['position'][1]
            crit.direction = c['direction']
            crit.age = c['age']
            crit.gender = c['gender']

            self.addCritter(crit)

        self.totalPopulation = allData['totalPop']
        self.livingPopulation = allData['livingPop']
        
        # jumbles name files and reinitialize name generators, and makes sure to check
        # for existence
        self.nameFlag = True
        self.jumbleNames('male.txt')
        self.jumbleNames('female.txt')
        self.maleNameGenerator = self.getValidName('male')
        self.femaleNameGenerator = self.getValidName('female')

        return True

    def saveWorld(self):
        '''
        Saves world data in encoded json string to fileLoc, a string ending with .txt
        '''
        allData = {'genepool':self.genepool, 'critters':{}, 'livingPop':self.livingPopulation,
                    'totalPop':self.totalPopulation}

        for n, c in self.critters.iteritems():
            crit = c.saveItself()
            allData['critters'][n] = crit

        with open(self.fileloc, 'w') as saveFile:
            json.dump(allData, saveFile, indent=4)

    def clearWorld(self):
        '''
        Wipes the world. List of names are re-shuffled and name generators reinitialized.
        '''
        self.genepool = config.defaultGenepool
        self.critters = {}
        self.livingPopulation = 0
        self.totalPopulation = 0
        self.jumbleNames('male.txt')
        self.jumbleNames('female.txt')

        self.maleNameGenerator = self.getValidName('male')
        self.femaleNameGenerator = self.getValidName('female')

        self.nameFlag = False
        self.fileloc = "untitled"

    def draw(self, screen):
        '''
        draws all the critters in the world
        '''
        for critterName in self.critters:
            self.critters[critterName].draw(screen)

    def findValidLoc(self, childCritter, parentCritter):
        '''
        Returns (x, y) coordinates closest to parentCritter where childCritter will fit without
        touching any other critters. If no parentCritter is given, returns a random location
        that is valid.
        '''
        # compute potentially valid locations for child and place in random order
        childRad = int(15 * childCritter.phenotypes['size'] / 2)
        dist = int(childRad + (15 * parentCritter.phenotypes['size']) / 2) + 5
        x = parentCritter.x
        y = parentCritter.y
        
        potentialLocs = [(x + dist, y + dist), (x + dist, y - dist), (x - dist, y + dist), (x - dist, y - dist),
                         (x, y + dist), (x, y - dist), (x + dist, y), (x - dist, y)]
        random.shuffle(potentialLocs)

        # return first location confirmed to be valid
        valid = True
        for loc in potentialLocs:
            # ensure child does not form off edge of world
            if (loc[0] < config.worldX0 or loc[0] > config.worldX or
               loc[1] < config.worldY0 or loc[1] > config.worldY):
                continue
            valid = True
            
            # check for collisions with existing critters
            for critterName in self.critters:
                critter = self.critters[critterName]
                rad = childRad + int(15 * critter.phenotypes['size'] / 2)
                dist = math.sqrt((critter.x - loc[0]) ** 2 + (critter.y - loc[1]) ** 2) - 5
                if dist < 0:
                    valid = False
                    break
            
            # check for collisions with critters born this round
            if valid:
                for critter in self.newCritters:
                    rad = childRad + int(15 * critter.phenotypes['size'] / 2)
                    dist = math.sqrt((critter.x - loc[0]) ** 2 + (critter.y - loc[1]) ** 2) - 5
                    if dist < 0:
                        valid = False
                        break
                if valid:
                    return loc
        print "ERROR: could not find a valid location for new child critter"
        return (None, None)

    def moveCritter(self, critter):
        '''
        moves a single critter, reproduces when possible, and updates list of deaths/births that
        occured this step.
        '''
        children = []

        # update age and return critter as dead if too old
        critter.increaseAge()
        if critter.age > critter.phenotypes['lifespan']:
            self.deadCritters.append(critter)
            return

        # get all critters potentially touching this one,
        # then remove those that are not actually touching
        touchingCritters = self.getCloseCritters(critter, 5)

        # try to reproduce with extremely close critters
        if critter.gender == "female":
            for potentialMate in touchingCritters:
                # check for correct gender of mate, fertility, and population density,
                # then add child to world between parents if selected
                if (potentialMate.gender == "male" and
                   random.random() < critter.phenotypes['fertility'] and
                   len(self.getCloseCritters(critter, 30)) < config.maxPopDensity):
                    # generates valid name for child according to gender and checks
                    # if the list of names is exhausted
                    name = "another breakable banana"
                    child = Critter(name, critter.genotype, potentialMate.genotype, (None, None))
                    
                    gender = child.gender
                    if gender == 'male':
                        name = next(self.maleNameGenerator, -1)
                        if name == -1:
                            self.maleNameGenerator = self.getValidName('male')
                            self.nameFlag = True
                            name = next(self.maleNameGenerator)

                    if gender == 'female':
                        name = next(self.femaleNameGenerator, -1)
                        if name == -1:
                            self.femaleNameGenerator = self.getValidName('female')
                            self.nameFlag = True
                            name = next(self.femaleNameGenerator)

                    child.name = name

                    newLoc = self.findValidLoc(child, critter)
                    # add critter to population if valid location found
                    if newLoc != (None, None):
                        child.x = newLoc[0]
                        child.y = newLoc[1]
                        self.newCritters.append(child)
                        children.append(child)

        # update goal direction when time passed exceeds attention span or if bumping into another critter
        if (len(touchingCritters) > 0) or (critter.attnTime > critter.phenotypes['attnspan']):
            critter.attnTime = 0
            closeCritters = self.getCloseCritters(critter, critter.phenotypes["vision"])
            
            # set direction to center of critter cluster
            closeCritters = self.getCloseCritters(critter, critter.phenotypes['vision'])
            clusterDirection = self.getCritterClusterDirection(closeCritters, critter.name)
            
            # reverse direction if too many critters present
            if len(closeCritters) > critter.phenotypes['socialness']:
                clusterDirection *= -1
            
            critter.direction = clusterDirection + random.uniform(-math.pi, math.pi) / 4
            
            # check that new direction is valid and randomly retry until it is or run out of tries
            valid = False
            attempts = 1
            oldX = critter.x
            oldY = critter.y
            
            while not valid and attempts < 10:
                # try a direction    
                critter.direction = clusterDirection + random.uniform(-math.pi, math.pi) / 4
                critter.move()
                if len(self.getCloseCritters(critter, 0)) == 0:
                    valid = True
                else:
                    attempts += 1
                # reset location
                critter.x = oldX
                critter.y = oldY
            # give a random direction if no directed one was valid
            if attempts == 10:
                critter.direction = random.uniform(-math.pi, math.pi)
        
        # update critter location
        oldX = critter.x
        oldY = critter.y
        critter.move()
        # undo movement if overlapping with new child
        for child in self.newCritters:
            if critter in self.getCloseCritters(child, 0):
                critter.x = oldX
                critter.y = oldY

    def moveCritters(self):
        '''
        moves all the critters in the world and updates their death/births
        '''
        newCritters = []
        deadCritters = []
        
        for critterName in self.critters:
            # move critter
            critter = self.critters[critterName]
            oldX = critter.x
            oldY = critter.y
            oldDir = critter.direction
            self.moveCritter(critter)
            # unmove if spot already taken
            if len(self.getCloseCritters(critter, 0)) > 0:
                critter.x = oldX
                critter.y = oldY
        
        # update critter population
        for newCritter in self.newCritters:
            self.addCritter(newCritter)
        for deadCritter in self.deadCritters:
            self.killCritter(deadCritter.name)
        self.newCritters = []
        self.deadCritters = []

    def getCritter(self, critName):
        '''
        Returns string containing information of critter of name critName
        '''
        if self.critters.has_key(critName):
            crit = self.critters[critName]
            data = crit.phenotypes

            string = 'Name: ' + crit.name + '\n' + 'gender: ' + crit.gender \
                + '\n' + 'age: ' + str(crit.age) + '\n'

            for gene, val in data.iteritems():
                string = string + gene + ': ' + str(val)
                if gene is 'color' or gene is 'shape':
                    both = crit.genotype.get(gene)
                    recessive = both[0].value
                    if both[0].dominance > both[1].dominance:
                        recessive = both[1].value
                    string = string + ' (' + str(recessive) + ')'
                string = string + '\n'
            
            return string
        return None

    def getStats(self):
        '''
        Returns a string of world stats to be printed including gender, color, shape 
        genes percentages, and population info.
        '''
        data = {'color':{}, 'shape':{}, 'socialness':[]}
        totals = {'color':0, 'shape':0, 'socialness':0}
        girls = 0
        boys = 0

        
        for n, c in self.critters.iteritems():
            # tallying color genes
            myColors = c.genotype.getGenes()['color']
            color1 = myColors[0].value
            color2 = myColors[1].value
            if data['color'].has_key(color1):
                data['color'][color1] += 1
            else:
                data['color'][color1] = 1
            if data['color'].has_key(color2):
                data['color'][color2] += 1
            else:
                data['color'][color2] = 1

            # tallying shape genes
            myShapes = c.genotype.getGenes()['shape']
            shape1 = myShapes[0].value
            shape2 = myShapes[1].value
            
            if data['shape'].has_key(shape1):
                data['shape'][shape1] += 1
            else:
                data['shape'][shape1] = 1
            if data['shape'].has_key(shape2):
                data['shape'][shape2] += 1
            else:
                data['shape'][shape2] = 1

            # gender
            g = c.gender
            if g is 'male':
                boys += 1
            else:
                girls += 1

            totals['color'] += 2
            totals['shape'] += 2

            soc = c.phenotypes['socialness']
            totals['socialness'] += soc
            data['socialness'].append(soc)

        boyPercentage = 0.0
        girlPercentage = 0.0

        if boys != 0:
            boyPercentage = boys*100/(boys+girls)

        if girls != 0:
            girlPercentage = girls*100/(boys+girls)

        string = 'World Stats:' + '\n' + 'living population: ' + str(len(self.critters)) + \
            '\n' + 'total population: ' + str(self.totalPopulation) + '\n' + \
            'male: ' + str(boyPercentage) + '%    ' + \
            'female: ' + str(girlPercentage) + '%\n'

        social_avg = 0.0
        if len(data['socialness']) != 0:
            social_avg = totals['socialness']/float(len(data['socialness']))
        social_sum = 0.0
        for num in data['socialness']:
            social_sum += float((num - social_avg)**2)

        for stat, stuff in data.iteritems():
            string = string + '\n' + stat + '\n'
            if stat is not 'socialness':
                for val, count in stuff.iteritems():
                    stuff[val] = count*100/totals[stat]
                    string = string + '    ' + str(val) + ': ' + str(stuff[val]) + '%\n'

        string = string + '    average: %.02f' % social_avg
        if len(data['socialness']) != 1:
            string = string + '\n    standard deviation: %.02f' % float(social_sum/(len(data['socialness'])-1)) + '\n'
        else:
            string = string + '\n    standard deviation: 0.00\n'

        return string
