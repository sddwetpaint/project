import pygame
import random
import math
import datetime
import config
from genome import Genome
from gene import Gene
from roundedrect import RoundedRect
from pygame.locals import *


class Critter:
    def __init__(self, name, genome1, genome2 = None, pos = None):
        '''
        create a critter of the given name, specifying one or two parent genomes as dictionaries,
        and the (x, y) coordinates of its location (random coordinates supplied if omitted)
        '''
        # genotype
        if genome2 == None:
            self.genotype = genome1
        else:
            self.genotype = Genome(genome1, genome2)
        
        # phenotype
        self.phenotypes = self.genotype.getPhenotypes()

        # give random location if none is provided
        if pos is None:
            pos = (random.randrange(config.worldX0, config.worldX),
                    random.randrange(config.worldY0, config.worldY))
        
        # other data
        self.name = name
        self.age = 0
        self.x = pos[0]
        self.y = pos[1]
        self.direction = 0
        self.attnTime = self.phenotypes['attnspan']
        self.gender = random.choice(["male", "female"])
        self.selected = False

    def __str__(self):
        '''
        prints phenotype and gender information of critter
        '''
        text = ['Critter ' + self.name + '\n']
        for phenotype in self.phenotypes:
            text.append(str(phenotype) + ': ' + str(self.phenotypes[phenotype]) + '\n')
        text.append("gender:" + self.gender + '\n')
        return ''.join(text)

    def move(self):
        '''
        move in currently specified direction
        '''
        deltaX = int(self.phenotypes['speed'] * math.cos(self.direction))
        deltaY = int(self.phenotypes['speed'] * math.sin(self.direction))
        newX = self.x + deltaX
        newY = self.y + deltaY
        rad = int(15 * self.phenotypes['size'] / 2) + 1
        # ensure always on screen
        if newX + rad > config.worldX:
            self.attnTime = self.phenotypes['attnspan']
            newX = config.worldX - rad
        elif newX - rad < config.worldX0:
            self.attnTime = self.phenotypes['attnspan']
            newX = config.worldX0 + rad

        if newY + rad > config.worldY:
            self.attnTime = self.phenotypes['attnspan']
            newY = config.worldY - rad
        elif newY - rad < config.worldY0:
            self.attnTime = self.phenotypes['attnspan']
            newY = config.worldY0 + rad

        self.x = newX
        self.y = newY
    
    def increaseAge(self):
        '''
        increase age of critter, currently by [rather arbitrarily] incrementing
        also increment how long the critter has been performing its task
        '''
        self.attnTime += 1
        self.age += 1

    def saveItself(self):
        data = {'name':self.name, 'age':self.age, 'gender':self.gender, 
                'position':(self.x, self.y) ,'direction':self.direction, 
                'attnspan':self.attnTime, 'phenotype':self.phenotypes, 
                'genotype':{}}

        for genetype, genes in self.genotype.getGenes().iteritems():
            gene1 = genes[0]
            gene2 = genes[1]
            data['genotype'][genetype] = ((gene1.value, gene1.frequency, gene1.dominance),
                                        (gene2.value, gene2.frequency, gene2.dominance))

        return data

    def draw(self, screen):
        '''
        draws critter on to screen
        '''
        shape = self.phenotypes['shape']
        color = self.phenotypes['color']
        size = int(15 * self.phenotypes['size'])

        if shape == 'triangle':
            if self.selected:
                pygame.draw.polygon(screen, (160, 230, 240), (
                    (self.x, self.y - (size / 2) - 3),
                    (self.x - 3 - (math.sqrt(3) * size / 4), self.y + (size / 4) + 3),
                    (self.x + 3 + (math.sqrt(3) * size / 4), self.y + (size / 4) + 3))
                )
            pygame.draw.polygon(screen, config.colors[color], (
                (self.x, self.y - (size / 2)),
                (self.x - (math.sqrt(3) * size / 4), self.y + (size / 4)),
                (self.x + (math.sqrt(3) * size / 4), self.y + (size / 4)))
            )
        elif shape == 'square':
            if self.selected:
                rec = pygame.Rect(self.x - (size / 2) - 3, self.y - (size / 2) - 3, size + 6, size + 6)
                pygame.draw.rect(screen, (160, 230, 250), rec)
            rectangle = pygame.Rect(self.x - (size / 2), self.y - (size / 2), size, size)
            pygame.draw.rect(screen, config.colors[color], rectangle)
        elif shape == 'circle':
            if self.selected:
                pygame.draw.circle(screen, (160, 230, 250), (self.x, self.y), int(size/2) + 3)
            pygame.draw.circle(screen, config.colors[color], (self.x, self.y), int(size/2))
        elif shape == 'rounded square':
            if self.selected:
                rec = pygame.Rect(self.x - (size / 2) - 3, self.y - (size / 2) - 3, size + 6, size + 6)
                RoundedRect(screen, (160, 230, 250), rec)
            rectangle = pygame.Rect(self.x - (size / 2), self.y - (size / 2), size, size)
            RoundedRect(screen, config.colors[color], rectangle)