import unittest
from genome import Genome
from critter import Critter
from world import World
import math
import config

class TestCritterClustering(unittest.TestCase):
    def setUp(self):
        self.world = World()
        self.critters = []
        self.names = ['will', 'the', 'critters', 'cluster', 'correctly']

        for i in range(0, 5):
            g = self.world.generateGenome()
            self.critters.append(Critter(self.names[i], g))
            self.world.addCritter(self.critters[i])
            self.critters[i].gender = 'male'

        self.critters[0].x = 100
        self.critters[0].y = 100
        self.critters[1].x = 100
        self.critters[1].y = 120
        self.critters[2].x = 180
        self.critters[2].y = 200
        self.critters[3].x = 190
        self.critters[3].y = 220
        self.critters[4].x = 190
        self.critters[4].y = 260

        #print "start"
        
        for critter1 in self.critters:
            for critter2 in self.critters:
                if critter1 != critter2:
                    dist = math.sqrt((critter1.x - critter2.x) ** 2 + (critter1.y - critter2.y) ** 2)
                    if dist < (critter1.phenotypes['size'] * 15/2) + (critter2.phenotypes['size'] * 15/2):
                        print 'fail'
        

        #self.assertEqual(self.world.totalPopulation, 5)


    '''def testFindNeighbors(self):
        for i in range(0, 5):
            closeCritters = self.world.getCloseCritters(critters[i], critters[i].phenotypes['vision'])
            for critter in closeCritters:
                dist = math.sqrt((critter.x - critters[i].x) ** 2 + (critter.y - critters[i].y) **2)
                visionRad = critters[i].phenotypes['vision'] + int(15 * critters[i].phenotypes['size']/2)
                critterRad = int(15 * critter.phenotypes['size']/2)
                distdist = visionRad + critterRad
                if dist <  
            #critters[i].phenotypes['vision']'''


    def testFindNeighbors(self):

        for critter1 in self.critters:
            closeCritters = self.world.getCloseCritters(critter1, critter1.phenotypes['vision'])
            critterVision = critter1.phenotypes['vision']

            estimateCritters = []

            for critter2 in self.critters:
                if critter1 != critter2:
                    #find distance between critter1 and critter2
                    dist = math.sqrt((critter1.x - critter2.x) ** 2 + (critter1.y - critter2.y) ** 2)
                    #check to see if the distance is within view of the critter
                    if critterVision + int(15 * critter1.phenotypes['size']/2) > dist - int(critter2.phenotypes['size'] * 15/2):
                        estimateCritters.append(critter2)

            for c in closeCritters:
                found = False
                for e in estimateCritters:
                    if c == e:
                        found = True
                self.assertTrue(found)

            for e in estimateCritters:
                found = False
                for c in closeCritters:
                    if c == e:
                        found = True
                self.assertTrue(found)

        print "Critters can find friends"
                        
    def testFindClusterDirection(self):
        for critter in self.critters:
            direction = self.world.getCritterClusterDirection(self.world.getCloseCritters(critter, critter.phenotypes['vision']), critter.name)
            closeCritters = self.world.getCloseCritters(critter, critter.phenotypes['vision'])
            sumX = 0
            sumY = 0
            for c in closeCritters:
                sumX += c.x
                sumY += c.y

            avgX = sumX / len(closeCritters)
            avgY = sumY / len(closeCritters)

            calcX = avgX - critter.x
            calcY = avgY - critter.y

            estimateDir = math.atan2(calcY,calcX)

            self.assertEqual(round(estimateDir%math.pi, 2), round(direction%math.pi, 2))

        print "Finding cluster direction passed"


    def testMove(self):
        for critter in self.critters:
            critter.direction = 0
            critter.phenotypes['speed'] = 3
            critter.attnTime = 0
            expeX = critter.x + 3
            expeY = critter.y
            self.world.moveCritter(critter)
            '''print expeX, expeY
            print critter.x, critter.y
            print'''
            self.assertTrue(expeX == critter.x)
            self.assertTrue(expeY == critter.y)

        print " Critters move correctly"

    def testEdgeOfWorld(self):
        braveCritter = self.critters[0]
        braveCritter.phenotypes['speed'] = 10
        braveCritter.attnTime = 0
        braveCritter.phenotypes['size'] = 1
        braveCritter.x = config.worldX - 10
        braveCritter.direction = 0
        self.world.moveCritter(braveCritter)
        self.assertTrue(braveCritter.x + 15/2 <= config.worldX)

        print "Brave Critters are afraid of the edge of the world"
        
                
                


if __name__ == "__main__":
    unittest.main()
