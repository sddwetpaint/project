# Gene class

class Gene:
    def __init__(self, name, val, freq, dom):
        '''
        creates a gene of the given gene type(name), value, frequency of occurence
        and level of dominance
        '''
        self.name = name
        self.value = val
        self.frequency = freq
        self.dominance = dom


    def copy(self):
        '''
        returns a duplicated gene object
        '''
        n = self.name
        v = self.value
        freq = self.frequency
        dom = self.dominance
        return Gene(n, v, freq, dom)

    def __str__(self):
        return 'name: ' + self.getName() + '\nvalue: ' + \
                str(self.getValue()) + '\nfrequency: ' + \
                str(self.getFrequency()) + '\ndominance: ' + \
                str(self.getDominance()) + '\n'
