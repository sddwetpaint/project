### Survival of the Critters ###

* This is a visual simulator of a population of geometric entities that lives in a closed environment that evolves according to the rules of genetic inheritance and user interference.
* Version 1.0

### How to run the simulator ###

* This game was created in PyGame with Python 2.7 32-bit. 
* For Windows:
    * We have created a .exe file using Py2exe
    * In the dist folder, double-click the gameloop.py
* For Ubuntu:
    * To run manually, install Python 2.7 32-bit and [PyGame](http://pygame.org/download.shtml)
    * Open a terminal window and navigate to the game folder. To run the game, enter:
    ```
    python gameloop.py
    ```