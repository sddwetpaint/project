import pygame
from pygame.locals import *
from button import Button

class Toggle(Button):
    '''
    Abstract Subclass of Button
    Additional private variable that indicates the toggle is "on" when turnedOn == True 
    and is "off" when turnedOn == False
    '''
    def __init__(self, width, height, x, y, color, world, surface, text, textColor=(255,255,255), textSize=10):
        Button.__init__(self, width, height, x, y, color, world, surface, text, textColor, textSize)
        self.__turnedOn = False

    def _action(self):
        # If the toggle was already on, stop the button function and turn off button
        if self.__turnedOn:
            self.__turnedOn = False
            return self._offAction()
        # If it was off, start the button function and turn the button on
        else:
            self.__turnedOn = True
            return self._onAction()
    
    def _onAction(self):
        '''
        Action to be made when toggle is turned "on". Meant to be overridden
        '''
        raise NotImplementedError()

    def _offAction(self):
        '''
        Action to be made when toggle is turned "off". Meant to be overridden
        '''
        raise NotImplementedError()

class SimpleToggle(Toggle):
    '''
    Subclass of Toggle: Implements a simple toggle button that returns true when on and false when off
    '''
    def __init__(self, width, height, x, y, color, world, surface, offtext, ontext=None, textColor=(255,255,255), textSize=10):
        Toggle.__init__(self, width, height, x, y, color, world, surface, offtext, textColor, textSize)
        self.__ontext = ontext
        self.__offtext = offtext

    def _onAction(self):
        self.setColor((222, 136, 134))
        if self.__ontext != None:
            self.setText(self.__ontext)
        return True

    def _offAction(self):
        self.setColor((150, 117, 171))
        if self.__ontext != None:
            self.setText(self.__offtext)
        return False