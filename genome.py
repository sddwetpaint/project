# Genome class
from gene import Gene
import config
import random


class Genome:
    def __init__(self):
        '''
        produce a blank genome
        '''
        self.genomeDict = {}

    def __init__(self, genome1=None, genome2=None):
        '''
        if one genome passed in, return a copy of the genome
        if two, simulate reproduction and create a genome from both
        '''
        # produce a blank genome if no base genome is provided
        self.genomeDict = {}
        
        # create a genome that selects a random gene for each gene type from the
        # two parent genomes passed in
        if genome1 is not None and genome2 is not None:
            self.genomeDict = {}
            genome1Dict = genome1.getGenes()
            genome2Dict = genome2.getGenes()
            
            for geneType in genome1Dict:
                gene1 = random.choice(genome1Dict[geneType]).copy()
                gene2 = random.choice(genome2Dict[geneType]).copy()
                self.genomeDict[geneType] = (gene1, gene2)
        
        # produce a copy of a single genome passed
        elif genome1 is not None:
            self.genomeDict = genome1.getGenes()
        else:
            pass


    def __str__(self):
        '''
        prints all genes inside the genome dictionary
        '''
        ans = ''
        for geneType, genes in self.genomeDict.iteritems():
            ans += geneType + ': \n'
            ans += str(self.genomeDict[geneType][0]) + str(self.genomeDict[geneType][1]) + '\n'

        return ans

    def getGenes(self):
        '''
        fetches the genome dictionary
        '''
        return self.genomeDict

    def get(self, geneType):
        '''
        fetches a list of genes of a particular type
        '''
        return self.genomeDict[geneType]

    def getPhenotypes(self):
        '''
        Generates dictionary of phenotypes from genes according to dominance and value
        '''
        phenotypes = {}
        for genetype, genes in self.genomeDict.iteritems():
            gene1 = genes[0]
            gene2 = genes[1]

            value = gene1.value

            if gene1.dominance < gene2.dominance:
                value = gene2.value
        
            elif gene1.dominance == gene2.dominance and gene1.dominance==0:
                value = (gene2.value+gene1.value)/2
            
            phenotypes[gene1.name] = value

        return phenotypes
