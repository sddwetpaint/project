import sys, pygame
from pygame.locals import *

class Window(object):
    '''
    class to make a window; deleting or adding this to the surface should 
    be done directly through the main surface object
    '''
    def __init__(self, width, height, x, y, color):
        self.__height = height
        self.__width = width
        self.__x = x
        self.__y = y
        self.__color = color
        self.__rect = Rect(self.__x, self.__y, self.__width, self.__height)
        self.__items = []
        
    def draw(self, screen):
        '''
        a function that draws the window (should be called through 
        surface object); it addtionally draws any items it contains
        '''
        pygame.draw.rect(screen, self.__color, self.__rect, 0)
        for item in self.__items:
            item.draw(screen)

    def getWidth(self):
        return self.__width

    def getHeight(self):
        return self.__height

    def getColor(self):
        return self.__color

    def getX(self):
        return self.__x

    def getY(self):
        return self.__y

    def setX(self, newX):
        self.__x = newX

    def changeColor(self, color):
        self.__color = color

    def addItem(self, newItem):
        '''
        adds items such as buttons and addtional screens (these will be 
        drawn when the draw function is called)
        '''

        #checks to see if the item has already been added
        for item in self.__items:
            if item == newItem:
                print 'ERROR: an item named ' + newItem + ' has already been added to this window'
                return

        self.__items.append(newItem)

    def deleteItem(self, itemName):
        '''
        function that deletes item from self.__items
        '''
        self.__items.remove(itemName)
